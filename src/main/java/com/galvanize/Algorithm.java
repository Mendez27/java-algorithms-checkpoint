package com.galvanize;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Algorithm
{
//    Within the Algorithm class, define a method named allEqual that:
//    Takes a String argument
//    Returns true if every letter in the string is the same (case-insensitive)
//    Otherwise returns false
    public static boolean allEqual(String input)
    {
        boolean trial = false;
        if(input.length() == 0)
        {
            return trial;
        }
        else if (input.length() == 1)
        {
            trial = true;
            return trial;
        }
        else
        {
           String first = input.toLowerCase();

            for (int i = 0; i < (input.length() - 1); i++)
            {

                if (first.charAt(i) == first.charAt(i+1))
                {
                    trial = true;
                }
                else
                {
                    trial = false;
                    break;
                }
            }
        }
        return trial;
    }

//    Within the Algorithm class, define a method named letterCount that:
//    Takes a String argument
//    Returns a Map of the form <String, Long> where:
//    the keys are the characters of the string (lowercase)
//    the values are the number of times those characters appear in the string (case-insensitive)
    public Map<String, Long> letterCount(String input)
    {
        Map<String, Long> temp = new HashMap<>();

        String first = input.toLowerCase();
        if (input.length() == 0)
        {
            return temp;
        }
        else
        {
            for (int i = 0; i < input.length(); i++) {
                String letter = Character.toString(first.charAt(i));
                long count = 1;

                if (temp.containsKey(letter)) {
                    count = temp.get(letter) + 1;

                }
                temp.put(letter, count);
            }
        }

        return temp;
    }
//    Within the Algorithm class, define a method named interleave that:
//    Takes two Lists of Strings (you can assume both arrays are of the same length).
//    Returns a string with the lists interleaved.
    public String interleave(List<String> list1, List<String> list2)
    {
        StringBuilder solve = new StringBuilder();
        for (int i = 0; i < list1.size(); i++)
        {
                solve.append(list1.get(i));
                solve.append(list2.get(i));
        }
        return solve.toString();
    }

}
