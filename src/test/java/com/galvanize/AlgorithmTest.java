package com.galvanize;


import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

public class AlgorithmTest
{
    @Test
    public void verifyIfTrueWhenEveryLetterInStringIsSame()
    {
        //Setup
        Algorithm test = new Algorithm();
        String ex = "aAa";
        String ex2 = "bbBbabbb";
        String ex3= "";
        String ex4 = "a";

        //Enact
        boolean meh = test.allEqual(ex);
        boolean meh2 = test.allEqual(ex2);
        boolean meh3 = test.allEqual(ex3);
        boolean meh4 = test.allEqual(ex4);
        //Assert
        assertTrue(meh);
        assertFalse(meh2);
        assertFalse(meh3);
        assertTrue(meh4);
    }

    @Test
    public void verifyThatStringArgumentReturnsCharactersAsKeysAndRepetitionsAsValues()
    {
        //Setup
        Algorithm test = new Algorithm();
        String ex = "aa";
        String ex2 = "abBcd";
        String ex3 = "";
        //Enact
       Map<String, Long> trial = test.letterCount(ex);
        Map<String, Long> trial2 = test.letterCount(ex2);
        Map<String, Long> trial3 = test.letterCount(ex3);
        Map<String,Long> test1 = new HashMap<>();
        test1.put("a",2L);
        Map<String,Long> test2 = new HashMap<>();
        test2.put("a",1L);
        test2.put("b",2L);
        test2.put("c",1L);
        test2.put("d",1L);
        Map<String,Long> test3 = new HashMap<>();
        //Assert
        assertEquals( test1 , trial  );
        assertEquals(test2, trial2);
        assertEquals(test3, trial3);
    }
    @Test
    public void verifyThatTwoListsOfStringsAreInterLeaved()
    {
        //Setup
        Algorithm test = new Algorithm();
        //Enact
        String ex = test.interleave(Arrays.asList("a", "b", "c"), Arrays.asList("d", "e", "f"));
        String ex2 = test.interleave(Arrays.asList("a", "c", "e"), Arrays.asList("b", "d", "f"));
        String ex3 = test.interleave(Collections.emptyList(),Collections.emptyList());
        //Assert
        assertEquals("adbecf",ex);
        assertEquals("abcdef", ex2);
        assertEquals("", ex3);

    }

}

    //Setup
    //Enact
    //Assert
